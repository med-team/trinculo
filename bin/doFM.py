#!/usr/bin/python

#  trinculo, Flexible tools for multinomial association
#    Copyright (C) 2014  Luke Jostins
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import math

if sys.argv[1] == "-l":
	maller=0;
	inputfile = sys.argv[2]
else:
	inputfile = sys.argv[1]
	maller=1

INFILE = open(inputfile)

header = INFILE.readline().split()
for i in range(len(header)):
	if maller == 0 and header[i] == "L+P1":
		break
	if maller == 1 and header[i] == "logBF":
		break

# first pass:

allMargins = []
maxMargin = float("-inf")
for rawline in INFILE:
	line = rawline.split()
	allMargins.append(float(line[i]))
	if float(line[i]) > maxMargin:
		maxMargin = float(line[i])

temp = 0
for M in allMargins:
	temp += math.exp(M - maxMargin)

PD = math.log(temp) + maxMargin

INFILE.close()
INFILE = open(inputfile)

print "\t".join(INFILE.readline().split()) + "\tP_CAUSAL"

for rawline in INFILE:
	line = rawline.split()
	print "\t".join(line) + "\t" + str(math.exp(float(line[i]) - PD))

