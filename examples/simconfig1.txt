Npheno: 4 #number of phenotypes (not including controls)
Nvar: 100 #number of variants
prevs: 0.96 0.01 0.01 0.01 0.01 #frequency of each phenotype (first entry is controls)
Ncounts: 2000 2000 2000 2000 2000 #number of individuals with each phenotype in sample (first entry is controls)
ORs: 1.2 1.2 1 1 #odds ratio of variant for each phenotype
f: 0.5 #risk allele frequency of variant
Ncovar: 5 #number of covariates
FreqConfound: 0 0.04 0 0.02 0 #effect of each covariate on variant (change in allele frequency per sd increase in covariate)
DiseaseConfound: #increase in log-odds for each phenotype per sd increase in each covariate (rows are covariates, columns are phenotypes)
1 0.5 1 0.5
0.5 1 0.5 0 
0.1 0.1 0.1 0.7 
0 0.1 0 0 
0 0 0 0
